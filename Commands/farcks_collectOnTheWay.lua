
function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to metal position",
		parameterDefs = {
            { 
				name = "farckUnits", 
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "farestStrongpoint", 
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "radius", 
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },


		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

function ClearState(self)
	self.haveorders = false
	self.toBeCollected = 0
end

function Run(self, units, parameter)
	
    local farestStrongpoint = parameter.farestStrongpoint
    local farckUnits = parameter.farckUnits
    local radius = parameter.radius
	
	--Spring.Echo(dump(closestDeadUnitSpot))
	
	-- pick the spring command implementing the reclaim
    local cmdID = CMD.RECLAIM
	
	--Define features to be collected
	local halfWidth = radius
	local halfHeitgh = radius
	
	--Check how many features with metal are left
	local listFeaturesArea = Spring.GetFeaturesInRectangle(farestStrongpoint.x-halfWidth, farestStrongpoint.z-halfHeitgh, farestStrongpoint.x+halfWidth, farestStrongpoint.z+halfHeitgh)
	self.toBeCollected = 0
	
	local restToBeCollected = false
	for k = 1, tablelength(listFeaturesArea) do 	
		local remainingMetal,_,_,_,_ = Spring.GetFeatureResources(listFeaturesArea[k])
		if remainingMetal > 0 then
			restToBeCollected = true
		end	
	end
	
	--Spring.Echo(dump(restToBeCollected))
	
	local pointman = farckUnits[1] -- while this is running, we know that #units > 0, so pointman is valid
	local pointX, pointY, pointZ = Spring.GetUnitPosition(pointman)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	
	-- check success
	
	if (not restToBeCollected) then
		if pointmanPosition ~= nil then
			bb.convoyPointmanPos = pointmanPosition
		end
		return SUCCESS
	
	-- give order to collect
	else
		if (not self.haveorders or (Spring.GetCommandQueue(units[1], 0) < 1)) then
			for i=1, #farckUnits do
				
				SpringGiveOrderToUnit(farckUnits[i], cmdID, {farestStrongpoint.x, farestStrongpoint.y, farestStrongpoint.z, 2*halfWidth}, {})
				
			end
			self.haveorders = true
		end
	end
		
	return RUNNING
end


function Reset(self)
	ClearState(self)
end
