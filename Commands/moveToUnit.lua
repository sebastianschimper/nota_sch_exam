
function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Move to absolute coordinates, specified by 'pos'. If the 'fight' checkbox is checked, then encountered enemy units are fought till death. ",
		parameterDefs = {
            { 
				name = "thisUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      { 
				name = "unitToMoveTo",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	
  local atlasUnit = parameter.thisUnit
  local unitToMoveTo = parameter.unitToMoveTo

  if not parameter.thisUnit then
		return FAILURE
	end
	
	if not parameter.unitToMoveTo then
		return FAILURE
  	end

	local x, y, z = Spring.GetUnitPosition(atlasUnit)
	local atlasPos = Vec3(x,y,z)
	local posx, posy, posz = Spring.GetUnitPosition(unitToMoveTo)
	local unitPos = Vec3(posx, posy, posz)
	
	local distance = atlasPos:Distance(unitPos)
	if distance < 50 then
		return SUCCESS
	end

	
	
	
	Spring.GiveOrderToUnit(atlasUnit, CMD.MOVE, {posx, posy, posz}, {})


	return RUNNING
		

end