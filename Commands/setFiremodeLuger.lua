
function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Set the firemode of the lugers ",
		parameterDefs = {
            { 
				name = "thisUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "firestate",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	
	if not parameter.thisUnits then
		return FAILURE
	end

	if not parameter.firestate then
		return FAILURE
	end

  	local lugers = parameter.thisUnits
	local firestate = parameter.firestate
	
		for i=1, #lugers do
			Spring.GiveOrderToUnit(lugers[i], CMD.FIRE_STATE, {firestate}, {})
		end


	return SUCCESS 


end