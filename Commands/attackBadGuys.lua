function getInfo()
    return {
        onNoUnits = SUCCESS,
        tooltip = "Set the firemode of the lugers ",
        parameterDefs = {
            {
                name = "thisUnits",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
            {
                name = "position",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
			},
			{
                name = "newCommandBool",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            },
        }
    }
end
 
-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0
 
local function ClearState(self)
    self.threshold = THRESHOLD_DEFAULT
    self.lastState = nil
end
 
function Run(self, units, parameter)
   
    local thisUnits = parameter.thisUnits
	local position = parameter.position
	local newCommandBool = parameter.newCommandBool
 
    if not parameter.position then
        return FAILURE
    end
 
    if not parameter.thisUnits then
        return FAILURE
    end
 
    local unitsInEnemyPos = Spring.GetUnitsInSphere(position.x, position.y, position.z, 25);
	if unitsInEnemyPos == nil then -- changed from amount
		for i=1, #thisUnits do
            Spring.GiveOrderToUnit(thisUnits[i], CMD.FIRE_STATE, {2}, {})
        end
        return SUCCESS
    end
   
	if (self.lastState == nil or newCommandBool == true) then
		newCommandBool = false;

        for i=1, #thisUnits do
            Spring.GiveOrderToUnit(thisUnits[i], CMD.FIRE_STATE, {0}, {})
            Spring.GiveOrderToUnit(thisUnits[i], CMD.ATTACK, {position.x, position.y, position.z}, {})
        end
    end
 
    self.lastState = RUNNING
    return RUNNING
 
end