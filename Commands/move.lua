function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Move to absolute coordinates, specified by 'pos'. If the 'fight' checkbox is checked, then encountered enemy units are fought till death. ",
		parameterDefs = {
            { 
				name = "thisUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      { 
				name = "pos",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	
  local atlasUnit = parameter.thisUnit
  local pos = parameter.pos

  if not parameter.thisUnit then
		return FAILURE
	end
	
	if not parameter.pos then
		return FAILURE
  	end

	if  parameter.pos == nil then
		return FAILURE
  	end


	local x, y, z = Spring.GetUnitPosition(atlasUnit)
	
	local distance = math.sqrt((x - pos.x) * (x - pos.x) + (z - pos.z) * (z - pos.z))
	if distance < 50 then
		return SUCCESS
	end

	-- check if unit is dead
	
	
	Spring.GiveOrderToUnit(atlasUnit, CMD.MOVE, {pos.x, pos.y, pos.z}, {})


	return RUNNING
		

end