
function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Set the firemode of the lugers ",
		parameterDefs = {
			{ 
				name = "thisUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
            { 
				name = "targetUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	
	local thisUnits = parameter.thisUnits
  	local targetUnit = parameter.targetUnit

  	if not parameter.targetUnit then
		return FAILURE
	end

	if not parameter.thisUnits then
		return FAILURE
    end
    
    if Spring.ValidUnitID(targetUnit) == false then
        return SUCCESS
    end
	
	
	for i=1, #thisUnits do
		Spring.GiveOrderToUnit(thisUnits[i], CMD.ATTACK, {targetUnit}, {})
	end


	return RUNNING

end