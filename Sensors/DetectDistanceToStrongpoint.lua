local sensorInfo = {
	name = "Detect Distance to Strongpoint",
	desc = "Detect Distance to Strongpoint",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end



return function(thisUnit, myStrongpoint)

    if thisUnit == nil then
        return nil
    end

    if myStrongpoint == nil then
        return nil
    end

    local x,y,z = Spring.GetUnitPosition(thisUnit)

    local unitPos = Vec3(x,y,z)
    
    local distance = unitPos:Distance(myStrongpoint)

    return distance

end