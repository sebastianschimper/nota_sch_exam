local sensorInfo = {
	name = "TeamUnitCountByName",
	desc = "Return number of units of given type in whole team",
	author = "PepeAmpere",
	date = "2017-05-14",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetTeamUnitDefCount = Spring.GetTeamUnitDefCount
local myTeamID = Spring.GetMyTeamID()

-- @description return energy resource info
-- @argument unitName [string] unique unit name
return function(unitName)
	local unitDefID = UnitDefNames[unitName].id
	return SpringGetTeamUnitDefCount(myTeamID, unitDefID)
end