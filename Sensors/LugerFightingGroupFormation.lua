local sensorInfo = {
	name = "pw",
	desc = "Done for Testing.",
	author = "Sebastian Schimper",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end



return function(lugers)
    local foramtion = {}

    local delta = 25
    local negInd = (-1) * #lugers
    
    foramtion[1] = Vec3( delta ,0,0)

    local current = 1

    for i=negInd, 0 do 
        foramtion[#foramtion + 1] = Vec3(i, 0, 0)

    end


	return foramtion
end