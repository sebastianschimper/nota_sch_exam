local sensorInfo = {
	name = "Detect Frack Units",
	desc = "Filters Frack Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(unitsArg)

    if unitsArg == nil then
        return nil
    end

    local validUnits = {}

    for i=1, #unitsArg do
        local thisUnitID = unitsArg[i]
        
        if Spring.GetUnitIsDead(thisUnitID) == nil then
            validUnits[#validUnits + 1] = thisUnitID
        else
            validUnits[#validUnits + 1] = nil
        end
    end

    return validUnits

end