local sensorInfo = {
	name = "Detect Luger Units",
	desc = "Filters Luger Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

function has_value(tbl, item)
    for key, value in pairs(tbl) do
        if value == item then 
            return true 
        end
    end
    return false
end


return function(enemies1pos, enemies2pos)

    --local enemies1pos = {}
    --local enemies2pos = {}
    local standStillEnemies = {}

    --[[
    for i=1, #enemies1 do
        local thisUnitID = enemies1[i]
        local x,y,z = Spring.GetUnitPosition(thisUnitID)
        local thisPos = Vec3(x,y,z)
        
        enemies1pos[#enemies1pos + 1] = thisPos
    end

    for j=1, #enemies2 do
        local thisUnitID = enemies2[j]
        local x,y,z = Spring.GetUnitPosition(thisUnitID)
        local thisPos = Vec3(x,y,z)

        enemies2pos[#enemies2pos + 1] = thisPos
    end
    --]]

    for k=1, #enemies2pos do
        local thisPos = enemies2pos[k]
        if has_value(enemies1pos, thisPos) == true then
            standStillEnemies[#standStillEnemies + 1] = thisPos
        end
    end

    return standStillEnemies

end