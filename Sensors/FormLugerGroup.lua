local sensorInfo = {
	name = "Detect Luger Units",
	desc = "Filters Luger Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(lugers, seers)

    local lugerGroup = {}


    --- assign ont seer to each group
    lugerGroup[#lugerGroup + 1] = seers[1]

    for i=1, 5 do
        local thisUnitID = lugers[i]
        
        lugerGroup[#lugerGroup + 1] = thisUnitID

    end

    return lugerGroup

end