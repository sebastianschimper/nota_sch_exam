local sensorInfo = {
	name = "Get Units Around Strongpoint",
	desc = "Get Units Around Strongpoint",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(strongpoint)

    if strongpoint == nil then
        return nil
    end

    local thisUnitsFiltered = {}
    
    local thisUnits = Spring.GetUnitsInSphere(strongpoint.x, strongpoint.y, strongpoint.z, 600)

    return thisUnits


end


