local sensorInfo = {
	name = "Detect Frack Units",
	desc = "Filters Frack Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end


function has_value(tbl, item)
    for key, value in pairs(tbl) do
        if value == item then 
            return true 
        end
    end
    return false
end

return function(unitsArg)

    if unitsArg == nil then
        return nil
    end

    local fightingUnits = {}

    for i=1, #unitsArg do
        local thisUnitID = unitsArg[i]
        local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
        
        if (UnitDefs[thisUnitDefID].name ~= "armfark" or UnitDefs[thisUnitDefID].name ~= "armpeep" or UnitDefs[thisUnitDefID].name ~= "armatlas" and has_value(unitsArg, thisUnitID)) then
            fightingUnits[#fightingUnits + 1] = thisUnitID
        end
    end

    return fightingUnits

end