local sensorInfo = {
	name = "Sense Random Point in Safe Area",
	desc = "Returns all units to rescure",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

local function randomPointInCircle(centerX, centerY, centerZ, R)

    local r = R * math.sqrt(math.random())
    local theta = math.random() * 2 * math.pi

    local thisX = centerX + r * math.cos(theta)
    local thisZ = centerZ + r * math.sin(theta)

    return Vec3(thisX, centerY, thisZ)
end

return function(safeAreaCenter, safeAreaRadius)

    local rndPtInArea = randomPointInCircle(safeAreaCenter.x, safeAreaCenter.y, safeAreaCenter.z, safeAreaRadius)

    return rndPtInArea

end