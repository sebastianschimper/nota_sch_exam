local sensorInfo = {
	name = "Detect Units by Name",
	desc = "Filters Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

-- pass table with units and unit name as string
return function(unitsArg, unitName)

    if unitsArg == nil then
        return nil
    end

    if unitName == nil then
        return nil
    end

    local filteredUnits = {}

    for i=1, #unitsArg do
        local thisUnitID = unitsArg[i]
        local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
        
        if (UnitDefs[thisUnitDefID].name ~= unitName and Spring.ValidUnitID(thisUnitID)) then
            filteredUnits[#filteredUnits + 1] = unitsArg[i]
        end

    end

    return filteredUnits

end