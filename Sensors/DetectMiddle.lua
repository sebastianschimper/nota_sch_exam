local sensorInfo = {
	name = "Detect Atlas Units",
	desc = "Filters Atlas Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end



return function(ownStrongpoint, enemyStrongPointsPos)

    if ownStrongpoint == nil then
        return nil
    end

    if enemyStrongPointsPos == nil then
        return nil
    end

    local middlePoint = Vec3((ownStrongpoint.x + enemyStrongPointsPos.x)/2, (ownStrongpoint.y + enemyStrongPointsPos.y)/2, (ownStrongpoint.z + enemyStrongPointsPos.z)/2)
    
    return middlePoint

end