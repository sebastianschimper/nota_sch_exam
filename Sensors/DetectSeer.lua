local sensorInfo = {
	name = "Detect Luger Units",
	desc = "Filters Luger Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(unitsArg)

    local seer = {}

    for i=1, #unitsArg do
        local thisUnitID = unitsArg[i]
        local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
        
        if (UnitDefs[thisUnitDefID].name == "armseer") then
            seer[#seer + 1] = unitsArg[i]
        end
    end

    return seer

end