local sensorInfo = {
	name = "Detect Atlas Units",
	desc = "Filters Atlas Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end



return function(thisUnits)

    local busy = false

    if thisUnits == nil then
        return nil
    end

    for i=1, #thisUnits do
        local commandQueue = Spring.GetUnitCommands(thisUnits[i], 1)
        if #commandQueue > 0 then
            busy = true
            break
        end
    end

    return busy

end