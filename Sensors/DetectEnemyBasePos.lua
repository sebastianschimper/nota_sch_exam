
local sensorInfo = {
	name = "Detect Enemy Base Center",
	desc = "Detect Enemy Base Center",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end


-- pass table with units and unit name as string
return function(enemyBuildings)

    if enemyBuildings ~= nil then
        local minDistance = math.huge
        local upperRightCornerPos = Vec3(Game.mapSizeX,0,0) 
        for i=1, #enemyBuildings do
            local x,y,z = Spring.GetUnitPosition(enemyBuildings[i])
            local thisPos = Vec3(x,y,z)
            local distance = upperRightCornerPos:Distance(thisPos)
            if distance <= minDistance then
                minDistance = distance
            end
        end


        for j=1, #enemyBuildings do
            local x,y,z = Spring.GetUnitPosition(enemyBuildings[j])
            local thisPos = Vec3(x,y,z)
            local distance = upperRightCornerPos:Distance(thisPos)
            if distance == minDistance then
                powerPlantInCenter = thisPos
            end
        end

        
        return powerPlantInCenter
    
    else
        return nil
    end
    
    
end
   