local sensorInfo = {
	name = "Get Units Around Strongpoint",
	desc = "Get Units Around Strongpoint",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(unitsArg)

    if unitsArg == nil then
        return nil
    end

    Spring.Echo(unitsArg)
    local x,y,z = Spring.GetUnitPosition(unitsArg)
    local posAsVec = Vec3(x,y,z)
    
    

    return posAsVec


end


