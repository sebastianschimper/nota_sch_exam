local sensorInfo = {
	name = "Detect Atlas Units",
	desc = "Filters Atlas Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end



return function( myBase, allEnemyStrongPointsPos)

    local farestEnemyStrongpoint = nil

    if myBase == nil then
        return nil
    end

    if allEnemyStrongPointsPos == nil then
        return nil
    end

    local minDistance = math.huge
    for j=1, #allEnemyStrongPointsPos do
        local thisPos = allEnemyStrongPointsPos[j].position
        local distance = myBase:Distance(thisPos)
        if distance <= minDistance then
            minDistance = distance
        end
    end

    for k=1, #allEnemyStrongPointsPos do
        local thisPos = allEnemyStrongPointsPos[k].position
        local distance = myBase:Distance(thisPos)
        if distance == minDistance then
            farestEnemyStrongpoint = allEnemyStrongPointsPos[k]
        end
    end

    return farestEnemyStrongpoint

end