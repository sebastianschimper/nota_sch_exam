local sensorInfo = {
	name = "Detect Atlas Units",
	desc = "Filters Atlas Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

function has_value(tbl, item)
    for key, value in pairs(tbl) do
        if value == item then 
            return true 
        end
    end
    return false
end

return function(enemyBasePos)

    if enemyBasePos == nil then
        return nil
    end

    local radius = 2000

    local enemyTeams = Sensors.core.EnemyTeamIDs()
    local enemyUnits = {}
    local enemyUnitsInEnemyBase = {}

    for i=1, #enemyTeams do
		local teamID = enemyTeams[i]
		local thisTeamUnits = Spring.GetTeamUnits(teamID)
		
		for u=1, #thisTeamUnits do
			enemyUnits[#enemyUnits + 1] = thisTeamUnits[u]
		end
    end

    local allUnitsInEnemyBase = Spring.GetUnitsInSphere(enemyBasePos.x, enemyBasePos.y, enemyBasePos.z, radius)

    for i=1, #allUnitsInEnemyBase do
        if has_value(enemyUnits, allUnitsInEnemyBase[i]) then
            enemyUnitsInEnemyBase[#enemyUnitsInEnemyBase + 1] = allUnitsInEnemyBase[i]
        end
    end

    return enemyUnitsInEnemyBase
end