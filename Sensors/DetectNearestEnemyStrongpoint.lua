local sensorInfo = {
	name = "Detect Atlas Units",
	desc = "Filters Atlas Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end



return function(corridor, myStrongpoint, allEnemyStrongPointsPos)

    local nearestEnemyStrongpoint = nil

    if myStrongpoint == nil then
        return nil
    end

    if allEnemyStrongPointsPos == nil then
        return nil
    end

    local minDistance = 10000000000
    for j=1, #allEnemyStrongPointsPos do
        local thisPos = allEnemyStrongPointsPos[j].position
        local distance = myStrongpoint:Distance(thisPos)
        if distance <= minDistance then
            minDistance = distance
        end
    end

    for k=1, #allEnemyStrongPointsPos do
        local thisPos = allEnemyStrongPointsPos[k].position
        local distance = myStrongpoint:Distance(thisPos)
        if distance == minDistance then
            nearestEnemyStrongpoint = allEnemyStrongPointsPos[k]
        end
    end

    return nearestEnemyStrongpoint

end