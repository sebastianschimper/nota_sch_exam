local sensorInfo = {
	name = "Detect Luger Units",
	desc = "Filters Luger Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(center)

    local radius = 3000

    local unitsInBase = Spring.GetUnitsInSphere(center.x, center.y, center.z, radius)

    return unitsInBase

end