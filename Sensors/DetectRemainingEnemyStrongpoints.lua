local sensorInfo = {
	name = "Detect Atlas Units",
	desc = "Filters Atlas Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end



return function(corridor, myAllyID)

    local allEnemyStrongPointsPos = {}

    for i=1, #corridor do

        if corridor[i].isStrongpoint == true then

            local thisOwnerAllyID = corridor[i].ownerAllyID 

            if thisOwnerAllyID ~= myAllyID then
                allEnemyStrongPointsPos[#allEnemyStrongPointsPos + 1] = corridor[i]
            end

        end

    end

    return allEnemyStrongPointsPos

end