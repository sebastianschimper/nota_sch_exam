local sensorInfo = {
	name = "pw",
	desc = "Done for Testing.",
	author = "Sebastian Schimper",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end



return function()
    local formation = {}
    
    for i=1, 5 do
        formation[#formation + 1] = Vec3( (i-1) * 20 ,0,0)
    end

    --[[
    foramtion[1] = Vec3(0,0,0)
    foramtion[2] = Vec3(100,0,0)
    foramtion[3] = Vec3(200,0,0)
    foramtion[4] = Vec3(300,0,0)
    foramtion[5] = Vec3(400,0,0)
    foramtion[6] = Vec3(500,0,0)

    ]]--

	return formation
end