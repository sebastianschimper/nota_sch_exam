local sensorInfo = {
	name = "Detect Atlas Units",
	desc = "Filters Atlas Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end



return function(shikas)

    if shikas == nil then
        return nil
    end

    local shikasUpdate = {}

    for i=1, #shikas do
        if Spring.ValidUnitID(shikas[i]) then
            shikasUpdate[#shikasUpdate + 1] = shikas[i]
        end
    end

    return shikasUpdate[1]


end