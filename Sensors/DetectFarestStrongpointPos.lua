local sensorInfo = {
	name = "Detect Atlas Units",
	desc = "Filters Atlas Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end



return function(corridor, basePos ,myAllyID)

    local allOwnStrongPointsPos = {}
    local farestStrongpoint = nil

    for i=1, #corridor do

        if corridor[i].isStrongpoint == true then

            local thisOwnerAllyID = corridor[i].ownerAllyID 

            if thisOwnerAllyID == myAllyID then
                allOwnStrongPointsPos[#allOwnStrongPointsPos + 1] = corridor[i].position
            end

        end

    end

    local maxDistance = 0
    for j=1, #allOwnStrongPointsPos do
        local distance = basePos:Distance(allOwnStrongPointsPos[j])
        if distance >= maxDistance then
            maxDistance = distance
        end
    end

    for k=1, #allOwnStrongPointsPos do
        local distance = basePos:Distance(allOwnStrongPointsPos[k])
        if distance == maxDistance then
            farestStrongpoint = allOwnStrongPointsPos[k]
        end
    end

    return farestStrongpoint

end