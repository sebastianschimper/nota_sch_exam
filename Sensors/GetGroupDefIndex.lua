local sensorInfo = {
	name = "pw",
	desc = "Done for Testing.",
	author = "Sebastian Schimper",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function reverse (arr)
	local i, j = 1, #arr

	while i < j do
		arr[i], arr[j] = arr[j], arr[i]

		i = i + 1
		j = j - 1
	end
end

return function(thisUnits)
    local table = {}

	local count = 0
	
    for i=1, #thisUnits, 1 do
        --local thisID = pewees[i]
        --count = count + 1
        table[thisUnits[i]] = i
    end

    --reverse(table)

	return table
end