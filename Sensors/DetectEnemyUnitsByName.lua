local sensorInfo = {
	name = "Detect Units by Name",
	desc = "Filters Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

function has_value(tbl, item)
    for key, value in pairs(tbl) do
        if value == item then 
            return true 
        end
    end
    return false
end

-- pass table with units and unit name as string
return function(unitsArg, unitName)

    if unitsArg == nil then
        return nil
    end

    if unitName == nil then
        return nil
    end

    local filteredUnits = {}
    local enemyTeams = Sensors.core.EnemyTeamIDs()
	local enemyUnits = {}
	
	for i=1, #enemyTeams do
		local teamID = enemyTeams[i]
		local thisTeamUnits = Spring.GetTeamUnits(teamID)
		
		for u=1, #thisTeamUnits do
			enemyUnits[#enemyUnits + 1] = thisTeamUnits[u]
		end
    end
    
    if enemyUnits ~= nil then
        for i=1, #enemyUnits do
            local thisUnitID = enemyUnits[i]
            local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
            
            if (UnitDefs[thisUnitDefID].name == unitName and Spring.ValidUnitID(thisUnitID) == true and has_value(unitsArg, thisUnitID)) then
                filteredUnits[#filteredUnits + 1] = thisUnitID
            else
                filteredUnits[#filteredUnits + 1] = nil
            end
        end
    end

    return filteredUnits

end