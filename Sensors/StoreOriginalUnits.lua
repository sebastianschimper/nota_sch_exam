local sensorInfo = {
	name = "StoreOriginalUnits",
	desc = "Store all units used during the game. For filtering Dead, alive and injured units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

function has_value(tbl, item)
    for key, value in pairs(tbl) do
        if value == item then 
            return true 
        end
    end
    return false
end

return function(originalUnits)

    local allCurrentTeamUnits = Spring.GetTeamUnits(Spring.GetMyTeamID())

    for i=1, #allCurrentTeamUnits do
        if has_value(originalUnits, allCurrentTeamUnits[i]) == false then
            originalUnits[#originalUnits + 1] = allCurrentTeamUnits[i]
        end
    end

    return originalUnits

end