local sensorInfo = {
	name = "Get Enemy Units Around Strongpoint",
	desc = "Get Enemy Units Around Strongpoint",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(thisUnit)

    local thisState = Spring.GetUnitStates(thisUnit)


    return thisState

end
