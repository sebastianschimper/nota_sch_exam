local sensorInfo = {
	name = "Detect Units by Name",
	desc = "Filters Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

-- pass table with units and unit name as string
return function(unitArg)

    if unitArg == nil then
        return nil
    end

    local shikaPos = nil

    if Spring.ValidUnitID(unitArg) then
        local x,y,z = Spring.GetUnitPosition(unitArg)
        shikaPos = Vec3(x,y,z)
    end
    

    return shikaPos

end