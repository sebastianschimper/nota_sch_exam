local sensorInfo = {
	name = "Detect Distance to Strongpoint",
	desc = "Detect Distance to Strongpoint",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end



return function(thisUnit, b)

    local x,y,z = Spring.GetUnitPosition(thisUnit)

    local unitPos = Vec3(x,y,z)
    
    local distance = unitPos:Distance(b)

    return distance

end