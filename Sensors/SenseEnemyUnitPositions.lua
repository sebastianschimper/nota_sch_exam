local sensorInfo = {
	name = "EnemySensor",
	desc = "Gives all enemy units in area",
	author = "Team 1, Group A",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function()
	local enemyTeams = Sensors.core.EnemyTeamIDs()
	local enemyUnits = {}
	local enemyUnitsPositions = {}
	
	for i=1, #enemyTeams do
		local teamID = enemyTeams[i]
		local thisTeamUnits = Spring.GetTeamUnits(teamID)
		
		for u=1, #thisTeamUnits do
			enemyUnits[#enemyUnits + 1] = thisTeamUnits[u]
		end
	end

	for k=1, #enemyUnits do
		local x,y,z = Spring.GetUnitPosition(enemyUnits[k])
		local thisPos = Vec3(x,y,z)

		enemyUnitsPositions[#enemyUnitsPositions + 1] = thisPos
	end
	
	return enemyUnitsPositions
end