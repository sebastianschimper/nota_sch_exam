local sensorInfo = {
	name = "Detect Luger Units",
	desc = "Filters Luger Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(zeuses)

    if zeuses == nil then
        return nil
    end

    local fightingGroup = {}


    --- assign ont seer to each group
    for i=1, #zeuses do
        fightingGroup[#fightingGroup + 1] = zeuses[i]
    end

    return fightingGroup

end