local sensorInfo = {
	name = "pw",
	desc = "Done for Testing.",
	author = "Sebastian Schimper",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end



return function()
    local foramtion = {}
    
    
    foramtion[1] = Vec3(0,0,0)
    foramtion[2] = Vec3(0,0,-10)
    foramtion[3] = Vec3(-10,0,-10)
    foramtion[4] = Vec3(10,0,-10)
    foramtion[5] = Vec3(0,0,-20)

    --[[
    foramtion[6] = Vec3(-10,0,-20)
    foramtion[7] = Vec3(10,0,-20)
    foramtion[8] = Vec3(0,0,-30)
    foramtion[9] = Vec3(-10,0,-30)
    foramtion[10] = Vec3(10,0,-30)
    --]]


	return foramtion
end