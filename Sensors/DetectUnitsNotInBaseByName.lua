local sensorInfo = {
	name = "Detect Luger Units",
	desc = "Filters Luger Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

function has_value(tbl, item)
    for key, value in pairs(tbl) do
        if value == item then 
            return true 
        end
    end
    return false
end

return function(unitsArg, unitsInBase, unitName)

    local lugerNotInBase = {}

    for i=1, #unitsArg do
        local thisUnitID = unitsArg[i]
        local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
        
        if (UnitDefs[thisUnitDefID].name == unitName) then
            if has_value(unitsInBase, thisUnitID) == false  then
                lugerNotInBase[#lugerNotInBase + 1] = unitsArg[i]
            end
        end
    end

    return lugerNotInBase

end