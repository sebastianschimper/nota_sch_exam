local sensorInfo = {
	name = "GetNumberOfUnits",
	desc = "Return number of units",
	author = "Sebastian Schimper",
	date = "2017-05-14",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(thisUnits)
	return #thisUnits
end