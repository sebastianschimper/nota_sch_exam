local sensorInfo = {
	name = "Detect Enemy Base Center",
	desc = "Detect Enemy Base Center",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

function has_value(tbl, item)
    for key, value in pairs(tbl) do
        if value == item then 
            return true 
        end
    end
    return false
end


-- pass table with units and unit name as string
return function(enemyBuildings)

    local enemyTeams = Sensors.core.EnemyTeamIDs()
    local enemyUnits = {}
	
	for i=1, #enemyTeams do
		local teamID = enemyTeams[i]
		local thisTeamUnits = Spring.GetTeamUnits(teamID)
		
		for u=1, #thisTeamUnits do
			enemyUnits[#enemyUnits + 1] = thisTeamUnits[u]
		end
    end
    
    if enemyUnits ~= nil then
        for i=1, #enemyUnits do
            local thisUnitID = enemyUnits[i]
            local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
            
            if (UnitDefs[thisUnitDefID].name == "corfus" and Spring.ValidUnitID(thisUnitID) == true and has_value(enemyBuildings, thisUnitID) == false) then
                enemyBuildings[#enemyBuildings + 1] = thisUnitID
            else
                enemyBuildings[#enemyBuildings + 1] = nil
            end
        end
    end

    return enemyBuildings
    
end