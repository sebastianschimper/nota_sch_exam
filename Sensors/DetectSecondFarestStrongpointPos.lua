local sensorInfo = {
	name = "Detect Atlas Units",
	desc = "Filters Atlas Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end



return function(corridor, currentSP ,myAllyID)

    local allOwnStrongPointsPos = {}
    local secondFarestStrongpoint = nil

    for i=1, #corridor do

        if corridor[i].isStrongpoint == true then

            local thisOwnerAllyID = corridor[i].ownerAllyID 

            if thisOwnerAllyID == myAllyID and corridor[i].position ~= currentSP then
                allOwnStrongPointsPos[#allOwnStrongPointsPos + 1] = corridor[i].position
            end

        end

    end

    local minDistance = 1000000
    for j=1, #allOwnStrongPointsPos do
        local distance = currentSP:Distance(allOwnStrongPointsPos[j])
        if distance <= minDistance then
            minDistance = distance
        end
    end

    local secondMinDistance  = minDistance + 1 

    for i=1, #allOwnStrongPointsPos do
        local distance = currentSP:Distance(allOwnStrongPointsPos[i])
        if distance <= secondMinDistance then
            minDistance = distance
        end
    end


    for k=1, #allOwnStrongPointsPos do
        local distance = currentSP:Distance(allOwnStrongPointsPos[k])
        if distance == minDistance then
            secondFarestStrongpoint = allOwnStrongPointsPos[k]
        end
    end

    return secondFarestStrongpoint

end