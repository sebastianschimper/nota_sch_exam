local sensorInfo = {
	name = "Detect Distance to Strongpoint",
	desc = "Detect Distance to Strongpoint",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end



return function(myStrongpoint, enemySP)

    
    local distance = enemySP:Distance(myStrongpoint)

    return distance

end