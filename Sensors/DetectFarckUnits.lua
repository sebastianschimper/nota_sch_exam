local sensorInfo = {
	name = "Detect Frack Units",
	desc = "Filters Frack Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(unitsArg)

    if unitsArg == nil then
        return nil
    end

    local farcks = {}

    for i=1, #unitsArg do
        local thisUnitID = unitsArg[i]
        local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
        
        if (UnitDefs[thisUnitDefID].name == "armfark") then
            farcks[#farcks + 1] = unitsArg[i]
        else
            farcks[#farcks + 1] = nil
        end
    end

    return farcks

end