local sensorInfo = {
	name = "Detect Units by Name",
	desc = "Filters Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

function has_value(tbl, item)
    for key, value in pairs(tbl) do
        if value == item then 
            return true 
        end
    end
    return false
end


-- pass table with units and unit name as string
return function(shikas)

    local enemyTeams = Sensors.core.EnemyTeamIDs()
	local enemyUnits = {}
	
	for i=1, #enemyTeams do
		local teamID = enemyTeams[i]
		local thisTeamUnits = Spring.GetTeamUnits(teamID)
		
		for u=1, #thisTeamUnits do
			enemyUnits[#enemyUnits + 1] = thisTeamUnits[u]
		end
    end
    
    if enemyUnits ~= nil then
        for i=1, #enemyUnits do
            local thisUnitID = enemyUnits[i]
            local thisUnitDefID = Spring.GetUnitDefID(thisUnitID)
            
            if (UnitDefs[thisUnitDefID].name == "shika" and has_value(shikas, thisUnitID) == false and Spring.ValidUnitID(thisUnitID) == true) then
                shikas[#shikas + 1] = thisUnitID
            else
                shikas[#shikas + 1] = nil
            end
        end
    end

    return shikas

end