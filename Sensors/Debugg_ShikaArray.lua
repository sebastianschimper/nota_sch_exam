local sensorInfo = {
	name = "Detect Units by Name",
	desc = "Filters Units",
	author = "Sebastian Schimper",
	date = "2019",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end


-- pass table with units and unit name as string
return function()

    local shikas = {}

    shikas[1] = 23108;
    shikas[2] = 2880;
    shikas[3] = 22001;
    shikas[4] = 23991;

    return shikas

end