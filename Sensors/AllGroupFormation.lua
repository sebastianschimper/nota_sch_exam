local sensorInfo = {
	name = "pw",
	desc = "Done for Testing.",
	author = "Sebastian Schimper",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end



return function(unitsArg)
    local formation = {}
    
    for i=1, #unitsArg do
        
        if i <= 10 then
            formation[#formation + 1] = Vec3(i, 0, 0)
        elseif i > 10 and i <= 20 then
            formation[#formation + 1] = Vec3(i%10, 0, 10)
        elseif i > 20 and i <= 30 then
            formation[#formation + 1] = Vec3(i%20, 0, 20)
        elseif i > 30 and i <= 40 then
            formation[#formation + 1] = Vec3(i%30, 0, 30)
        elseif i > 40 and i <= 50 then
            formation[#formation + 1] = Vec3(i%40, 0, 40)
        end

    end


	return formation
end